#include <assert.h>
#include <errno.h>
#include <string.h>

#include <utils.h>
#include <log/log.h>

#include "builtin/nvic/nvic.h"
#include "builtin/scb/scb.h"
#include "builtin/systick/systick.h"
#include "cpu.h"
#include "decoder/decoder.h"
#include "opcodes/thumb16/store.h"

static int cpu_exception_enter(struct cpu *cpu, enum exception exception, int tail_chaining);

int
cpu_init(struct cpu **dst, MemBus *bus, const char *name, uint32_t cpuid)
{
	struct cpu *self = calloc(1, sizeof(*self));
	if (!self) return ENOMEM;

	self->name = strdup(name);
	self->bus = bus;
	self->int_mgr = int_mgr_get(name);

	if (!self->int_mgr) return ENOENT;

	int_set_priority(self->int_mgr, EXC_RESET, -3);
	int_set_priority(self->int_mgr, EXC_NMI, -2);
	int_set_priority(self->int_mgr, EXC_HARD_FAULT, -1);

	/* Initialize internal peripherals */
	self->nvic = nvic_init(bus, self->int_mgr, name);
	self->scb = scb_init(bus, self->int_mgr, cpuid);
	self->systick = systick_init(self->bus, self->int_mgr);

	*dst = self;
	return 0;
}

int
cpu_deinit(struct cpu *self)
{
	int ret = 0;

	if (self->thread_running) {
		self->thread_running = false;
		pthread_join(self->tid, NULL);
	}

	/* Deinit internal peripherals */
	ret = nvic_deinit(self->nvic);
	if (ret) return ret;

	ret = scb_deinit(self->scb);
	if (ret) return ret;

	ret = systick_deinit(self->systick);
	if (ret) return ret;

	free(self->name);
	free(self);

	return ret;
}

int
cpu_reset(struct cpu *cpu)
{
	int ret = 0;

	memset(&cpu->regs, 0, sizeof(cpu->regs));

	ret |= mem_read(cpu->bus, EXC_ADDR(EXC_RESET),
	                &cpu->regs.r[REG_PC], sizeof(cpu->regs.r[REG_PC]));
	ret |= mem_read(cpu->bus, MSR_ADDR,
	                &cpu->regs.r[REG_SP], sizeof(cpu->regs.r[REG_SP]));

	cpu->regs.epsr.t = 1;
	cpu->regs.apsr.n = 0;
	cpu->regs.apsr.z = 0;
	cpu->regs.apsr.c = 0;
	cpu->regs.apsr.v = 0;
	cpu->regs.ipsr.isr = 0;

	cpu->regs.primask.primask = 0;
	cpu->regs.control.stack = 0;

	return ret;
}

int
cpu_step(struct cpu *cpu)
{
	int next_exc;
	uint32_t opcode;
	int ret = 0;

	/* Handle exception enter */
	if ((next_exc = int_get_next_pending(cpu->int_mgr)) >= 0) {
		cpu_exception_enter(cpu, next_exc, 0);
	}

	/* Fetch */
	ret = mem_execute(cpu->bus, cpu->regs.r[REG_PC] & ~0x1,
					  &opcode, sizeof(opcode));
	if (ret) return 1;

	/* Decode + execute */
	ret = execute(cpu, opcode);

	return ret;
}

int
cpu_exception_return(struct cpu *cpu)
{
	const size_t reg_size = sizeof(cpu->regs.r[0]);
	int next_exc;
	int ret = 0;
	uint32_t sp;
	uint32_t xpsr;

	/* Get the next pending exception */
	next_exc = int_get_next_pending(cpu->int_mgr);

	/* Perform tail chaining if another interrupt is pending */
	if (next_exc >= 0) {
		return cpu_exception_enter(cpu, next_exc, 1);
	}

	if (cpu->regs.r[REG_PC] & EXC_RETURN_PSP) {
		/* MSP -> PSP switch */
		sp = cpu->regs.psp;
		cpu->regs.control.stack = 1;
		cpu->regs.msp = cpu->regs.r[REG_SP];
	} else {
		sp = cpu->regs.r[REG_SP];
	}

	/* Pop stack frame */
	sp += 32;
	ret |= mem_read(cpu->bus, sp - 4, &xpsr, sizeof(xpsr));
	ret |= mem_read(cpu->bus, sp - 8, &cpu->regs.r[REG_PC], reg_size);
	ret |= mem_read(cpu->bus, sp - 12, &cpu->regs.r[REG_LR], reg_size);
	ret |= mem_read(cpu->bus, sp - 16, &cpu->regs.r[REG_R12], reg_size);
	ret |= mem_read(cpu->bus, sp - 20, &cpu->regs.r[REG_R3], reg_size);
	ret |= mem_read(cpu->bus, sp - 24, &cpu->regs.r[REG_R2], reg_size);
	ret |= mem_read(cpu->bus, sp - 28, &cpu->regs.r[REG_R1], reg_size);
	ret |= mem_read(cpu->bus, sp - 32, &cpu->regs.r[REG_R0], reg_size);

	if (xpsr_epsr_stkalign_get(xpsr)) {
		sp += sizeof(uint32_t);
	}

	cpu->regs.r[REG_SP] = sp;

	cpu->regs.apsr.n = xpsr_apsr_n_get(xpsr);
	cpu->regs.apsr.z = xpsr_apsr_z_get(xpsr);
	cpu->regs.apsr.c = xpsr_apsr_c_get(xpsr);
	cpu->regs.apsr.v = xpsr_apsr_v_get(xpsr);
	cpu->regs.epsr.t = xpsr_epsr_t_get(xpsr);
	cpu->regs.ipsr.isr = xpsr_ipsr_isr_get(xpsr);

	/* TODO MSP/PSP switch */
	return ret;
}


/* Static functions {{{ */
static int
cpu_exception_enter(struct cpu *cpu, enum exception exception, int tail_chaining)
{
	const size_t reg_size = sizeof(cpu->regs.r[0]);
	uint32_t sp, lr;
	uint32_t xpsr;
	int ret = 0;
	uint32_t ivt_entry_addr;

	assert(exception < NVIC_INT_COUNT);

	/* If already processing the same ISR, no need to enter again */
	if (cpu->regs.ipsr.isr == exception) {
		return 0;
	}

	if (!tail_chaining) {
		lr = EXC_RETURN;
		sp = cpu->regs.r[REG_SP];

		xpsr = 0;
		xpsr_apsr_n_set(&xpsr, cpu->regs.apsr.n);
		xpsr_apsr_z_set(&xpsr, cpu->regs.apsr.z);
		xpsr_apsr_c_set(&xpsr, cpu->regs.apsr.c);
		xpsr_apsr_v_set(&xpsr, cpu->regs.apsr.v);
		xpsr_epsr_t_set(&xpsr, cpu->regs.epsr.t);
		xpsr_ipsr_isr_set(&xpsr, cpu->regs.ipsr.isr);

		if (sp & 0x8) {
			/* Align stack to 64 bits, set bit 9 of xPSR */
			sp -= sizeof(uint32_t);
			xpsr_epsr_stkalign_set(&xpsr, 1);
		}

		/* Push stack frame */
		ret |= mem_write(cpu->bus, sp - 4, &xpsr, sizeof(xpsr));
		ret |= mem_write(cpu->bus, sp - 8, &cpu->regs.r[REG_PC], reg_size);
		ret |= mem_write(cpu->bus, sp - 12, &cpu->regs.r[REG_LR], reg_size);
		ret |= mem_write(cpu->bus, sp - 16, &cpu->regs.r[REG_R12], reg_size);
		ret |= mem_write(cpu->bus, sp - 20, &cpu->regs.r[REG_R3], reg_size);
		ret |= mem_write(cpu->bus, sp - 24, &cpu->regs.r[REG_R2], reg_size);
		ret |= mem_write(cpu->bus, sp - 28, &cpu->regs.r[REG_R1], reg_size);
		ret |= mem_write(cpu->bus, sp - 32, &cpu->regs.r[REG_R0], reg_size);
		sp -= 32;

		if (cpu->regs.control.stack) {
			/* PSP -> MSP switch */
			cpu->regs.psp = cpu->regs.r[REG_SP];
			sp = cpu->regs.msp;
			cpu->regs.control.stack = 0;
			lr |= EXC_RETURN_PSP;
		}

		if (!xpsr_ipsr_isr_get(xpsr)) {
			/* Thread -> handler switch */
			lr |= EXC_RETURN_THR;
		}

		/* Update LR+SP */
		cpu->regs.r[REG_SP] = sp;
		cpu->regs.r[REG_LR] = lr;
	}

	/* Mark as no longer pending while it's being serviced */
	cpu->regs.ipsr.isr = exception;
	int_set_pending(cpu->int_mgr, exception, 0);

	/* Launch exception handler */
	ivt_entry_addr = scb_get_vtor(cpu->scb) + EXC_ADDR(exception);
	ret |= mem_read(cpu->bus, ivt_entry_addr, &cpu->regs.r[REG_PC], 4);
	return ret;
}

/* }}} */

#pragma once

#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>

#include <module.h>
#include <gpio.h>
#include <memory.h>
#include <interrupts.h>
#include <utils.h>

#include "builtin/nvic/nvic.h"
#include "builtin/scb/scb.h"
#include "builtin/systick/systick.h"

#define EXC_RETURN 0xFFFFFFF1
#define EXC_RETURN_MSP 0x0
#define EXC_RETURN_PSP 0x4
#define EXC_RETURN_HND 0x0
#define EXC_RETURN_THR 0x8

#define EXC_ADDR(exc) ((exc) * sizeof(uint32_t))
#define MSR_ADDR 0x00000000

enum exception {
	EXC_NONE = 0,
	EXC_RESET = 1,
	EXC_NMI = 2,
	EXC_HARD_FAULT = 3,
	EXC_MEM_MANAGE = 4,
	EXC_BUS_FAULT = 5,
	EXC_USAGE_FAULT = 6,
	EXC_SVC = 11,
	EXC_PENDSV = 14,
	EXC_SYSTICK = 15,

	/* Start of external interrupts */
	EXC_INT_BASE = 16
};


struct apsr {
	uint8_t n : 2;      /* Negative */
	uint8_t z : 1;      /* Zero */
	uint8_t c : 1;      /* Carry */
	uint8_t v : 1;      /* Overflow */
	uint8_t q : 1;      /* Saturation */
};

struct epsr {
	uint8_t t : 1;      /* Thumb */
};

struct ipsr {
	uint8_t isr : 6;    /* ISR number */
};

struct primask {
	uint8_t primask : 1;    /* Enable/disable interrupts */
};

struct control {
	uint8_t stack : 1;      /* MSP/PSP switcher */
	uint8_t npriv : 1;      /* Thread mode privilege level */
};

REG_DEF(xpsr_apsr, 0xF, 28)
REG_DEF(xpsr_apsr_n, 0x1, 31)
REG_DEF(xpsr_apsr_z, 0x1, 30)
REG_DEF(xpsr_apsr_c, 0x1, 29)
REG_DEF(xpsr_apsr_v, 0x1, 28)
REG_DEF(xpsr_epsr_t, 0x1, 24)
REG_DEF(xpsr_epsr_stkalign, 0x1, 9)
REG_DEF(xpsr_ipsr_isr, 0x1F, 0)

enum regidx {
	REG_R0  = 0,
	REG_R1  = 1,
	REG_R2  = 2,
	REG_R3  = 3,
	REG_R4  = 4,
	REG_R5  = 5,
	REG_R6  = 6,
	REG_R7  = 7,
	REG_R8  = 8,
	REG_R9  = 9,
	REG_R10 = 10,
	REG_R11 = 11,
	REG_R12 = 12,
	REG_R13 = 13,
	REG_R14 = 14,
	REG_R15 = 15,
	REG_SP  = REG_R13,
	REG_LR  = REG_R14,
	REG_PC  = REG_R15,

	/* R16 = next program counter, not part of the actual core spec */
	REG_NEXT_PC = 16,
	REG_COUNT
};

struct registers {
	uint32_t r[REG_COUNT];

	uint32_t msp, psp;  /* Banked R13 */

	struct apsr apsr;
	struct epsr epsr;
	struct ipsr ipsr;
	struct primask primask;
	struct control control;
};

struct cpu {
	char *name;

	struct registers regs;

	MemBus *bus;
	InterruptManager *int_mgr;

	struct nvic *nvic;
	struct scb *scb;
	struct systick *systick;

	pthread_t tid;
	pthread_mutex_t exec_mutex;
	bool thread_running;
};

enum condition {
	COND_EQ = 0x00, /* Zero/equal */
	COND_CS = 0x01, /* Carry set */
	COND_MI = 0x02, /* Negative */
	COND_VS = 0x03, /* Overflow set */
	COND_HI = 0x04, /* Signed higher */
	COND_GE = 0x05, /* Unsigned greater/equal */
	COND_GT = 0x06, /* Unsigned greater */
	COND_AA = 0x07  /* Always */
};

bool condition_passed(struct cpu *cpu, enum condition cond);

#include "architecture.h"

bool
condition_passed(struct cpu *cpu, enum condition cond)
{
	enum condition base_condition = cond >> 1;
	bool invert = cond & 0x01;

	bool passed;

	switch (base_condition) {
	case COND_EQ:
		passed = cpu->regs.apsr.z;
		break;

	case COND_CS:
		passed = cpu->regs.apsr.c;
		break;

	case COND_MI:
		passed = cpu->regs.apsr.n;
		break;

	case COND_VS:
		passed = cpu->regs.apsr.v;
		break;

	case COND_HI:
		passed = cpu->regs.apsr.c && !cpu->regs.apsr.z;
		break;

	case COND_GE:
		passed = cpu->regs.apsr.n == cpu->regs.apsr.v;
		break;

	case COND_GT:
		passed = cpu->regs.apsr.n == cpu->regs.apsr.v && !cpu->regs.apsr.z;
		break;

	default:
		passed = true;
		break;
	}

	return passed ^ invert;
}

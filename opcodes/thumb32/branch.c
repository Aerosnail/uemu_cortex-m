#include "branch.h"

int
bl(struct cpu *cpu, int32_t imm)
{
	cpu->regs.r[REG_LR] = (cpu->regs.r[REG_PC] - 1) | 0x1;
	cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + imm;

	return 0;
}

#include "shift.h"

int
lsl_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] << shift;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	if (shift) cpu->regs.apsr.c = (cpu->regs.r[rn] >> (32 - shift)) & 0x1;

	return 0;
}

int
lsl_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return lsl_imm(cpu, rd, rn, cpu->regs.r[rm] & 0xFF);
}

int
lsr_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] >> shift;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	if (shift) cpu->regs.apsr.c = cpu->regs.r[rn] >> (shift - 1) & 0x1;

	return 0;
}

int
lsr_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return lsr_imm(cpu, rd, rn, cpu->regs.r[rm] & 0xFF);
}

int
asr_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = (int32_t)cpu->regs.r[rn] >> shift;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	if (shift) cpu->regs.apsr.c = cpu->regs.r[rn] >> (shift - 1) & 0x1;

	return 0;
}

int
asr_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{

	return asr_imm(cpu, rd, rn, cpu->regs.r[rm] & 0xFF);
}

int
rol_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] << shift | (shift ? cpu->regs.apsr.c : 0);

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	if (shift) cpu->regs.apsr.c = (cpu->regs.r[rn] >> (32 - shift)) & 0x1;

	return 0;
}

int
rol_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return rol_imm(cpu, rd, rn, cpu->regs.r[rm] & 0xFF);
}

int
ror_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] >> shift | (shift ? cpu->regs.apsr.c << 31 : 0);

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	if (shift) cpu->regs.apsr.c = cpu->regs.r[rn] >> (shift - 1) & 0x1;

	return 0;
}

int
ror_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return ror_imm(cpu, rd, rn, cpu->regs.r[rm] & 0xFF);
}


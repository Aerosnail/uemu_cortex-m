#pragma once

#include <stdint.h>

#include "architecture.h"

int adr(struct cpu *cpu, uint_fast8_t rd, uint32_t imm);

#include "move.h"

int
mov_imm(struct cpu *cpu, uint_fast8_t rd, uint32_t imm, uint_fast8_t flags)
{
	int result;
	rd += rd == REG_PC;

	result = imm;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
	}

	return 0;
}

int
mov_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm, uint_fast8_t flags)
{
	return mov_imm(cpu, rd, cpu->regs.r[rm], flags);
}

int
mvn_imm(struct cpu *cpu, uint_fast8_t rd, uint32_t imm, uint_fast8_t flags)
{
	int result;
	rd += rd == REG_PC;

	result = ~imm;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
	}

	return 0;
}

int
mvn_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm, uint_fast8_t flags)
{
	return mvn_imm(cpu, rd, cpu->regs.r[rm], flags);
}

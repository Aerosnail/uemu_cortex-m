#include "arith.h"

int
add_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];
	rd += rd == REG_PC;

	result = op1 + imm;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
		cpu->regs.apsr.c = result < op1;
		cpu->regs.apsr.v = !((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);
	}

	return 0;
}

int
add_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags)
{
	return add_imm(cpu, rd, rn, cpu->regs.r[rm], flags);
}

int
adc_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];
	rd += rd == REG_PC;

	result = op1 + imm + cpu->regs.apsr.c;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
		cpu->regs.apsr.c = result < op1 || (cpu->regs.apsr.c && result == op1);
		cpu->regs.apsr.v = !((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);
	}

	return 0;
}

int
adc_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags)
{
	return adc_imm(cpu, rd, rn, cpu->regs.r[rm], flags);
}

int
sub_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];
	rd += rd == REG_PC;

	result = op1 - imm;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
		cpu->regs.apsr.c = op1 >= imm;
		cpu->regs.apsr.v = ((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);
	}

	return 0;
}

int
sub_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags)
{
	return sub_imm(cpu, rd, rn, cpu->regs.r[rm], flags);
}

int
sbc_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];
	rd += rd == REG_PC;

	result = op1 - imm - !cpu->regs.apsr.c;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
		cpu->regs.apsr.c = result < op1 || (cpu->regs.apsr.c && result == op1);
		cpu->regs.apsr.v = ((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);
	}

	return 0;
}

int
sbc_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags)
{
	return sbc_imm(cpu, rd, rn, cpu->regs.r[rm], flags);
}

int
rsb_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];
	rd += rd == REG_PC;

	result = imm - op1;

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
		cpu->regs.apsr.c = imm >= op1;
		cpu->regs.apsr.v = ((op1 ^ imm) >> 31) && ((result ^ imm) >> 31);
	}

	return 0;
}

int
rsb_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags)
{
	return rsb_imm(cpu, rd, rn, cpu->regs.r[rm], flags);
}

int
mul(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] * cpu->regs.r[rm];

	cpu->regs.r[rd] = result;

	if (flags) {
		cpu->regs.apsr.n = result >> 31;
		cpu->regs.apsr.z = result == 0;
	}

	return 0;
}

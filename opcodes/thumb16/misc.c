#include "misc.h"

int
adr(struct cpu *cpu, uint_fast8_t rd, uint32_t imm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = (cpu->regs.r[REG_PC] & ~0x3) + imm;

	cpu->regs.r[rd] = result;

	return 0;
}

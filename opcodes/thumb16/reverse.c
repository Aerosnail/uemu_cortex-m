#include "reverse.h"

int
rev(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	uint32_t result;

	rd += rd == REG_PC;

	result = (cpu->regs.r[rm] >> 24 & 0x000000FF) |
	         (cpu->regs.r[rm] >> 8  & 0x0000FF00) |
	         (cpu->regs.r[rm] << 8  & 0x00FF0000) |
	         (cpu->regs.r[rm] << 24 & 0xFF000000);

	cpu->regs.r[rd] = result;
	return 0;
}

int
rev16(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	uint32_t result;

	rd += rd == REG_PC;

	result = (cpu->regs.r[rm] >> 8  & 0x00FF00FF) |
	         (cpu->regs.r[rm] << 8  & 0xFF00FF00);

	cpu->regs.r[rd] = result;
	return 0;
}

int
revsh(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	int32_t result;

	rd += rd == REG_PC;

	result = (cpu->regs.r[rm] << 24 & 0xFF000000) |
	         (cpu->regs.r[rm] << 8  & 0x00FF0000);
	result >>= 16;

	cpu->regs.r[rd] = result;

	return 0;
}


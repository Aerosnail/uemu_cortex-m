#pragma once

#include <stdint.h>
#include "architecture.h"

int mov_imm(struct cpu *cpu, uint_fast8_t rd, uint32_t imm, uint_fast8_t flags);
int mov_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm, uint_fast8_t flags);
int mvn_imm(struct cpu *cpu, uint_fast8_t rd, uint32_t imm, uint_fast8_t flags);
int mvn_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm, uint_fast8_t flags);

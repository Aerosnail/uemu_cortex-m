#pragma once

#include <stdint.h>

#include "architecture.h"

int rev(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);
int rev16(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);
int revsh(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);

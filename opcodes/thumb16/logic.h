#pragma once

#include <stdint.h>
#include "architecture.h"

int and_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm);
int and_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);
int orr_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm);
int orr_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);
int xor_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm);
int xor_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);
int bic_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm);
int bic_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);

#include "extend.h"

int
sxth(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = (((int32_t)cpu->regs.r[rm]) << 16) >> 16;

	cpu->regs.r[rd] = result;

	return 0;
}

int
sxtb(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = (((int32_t)cpu->regs.r[rm]) << 24) >> 24;

	cpu->regs.r[rd] = result;

	return 0;
}

int
uxth(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = (((uint32_t)cpu->regs.r[rm]) << 16) >> 16;

	cpu->regs.r[rd] = result;

	return 0;
}

int
uxtb(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = (((uint32_t)cpu->regs.r[rm]) << 24) >> 24;

	cpu->regs.r[rd] = result;

	return 0;
}


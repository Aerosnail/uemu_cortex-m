#include "logic.h"

int
and_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] & imm;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	return 0;
}

int
and_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return and_imm(cpu, rd, rn, cpu->regs.r[rm]);
}

int
orr_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] | imm;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	return 0;
}

int
orr_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return orr_imm(cpu, rd, rn, cpu->regs.r[rm]);
}

int
xor_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm)
{
	uint32_t result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] ^ imm;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	return 0;
}

int
xor_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return xor_imm(cpu, rd, rn, cpu->regs.r[rm]);
}

int
bic_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm)
{
	int result;
	rd += rd == REG_PC;

	result = cpu->regs.r[rn] & ~imm;

	cpu->regs.r[rd] = result;
	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	return 0;
}

int
bic_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm)
{
	return bic_imm(cpu, rd, rn, cpu->regs.r[rm]);
}

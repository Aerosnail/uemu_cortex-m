#pragma once

#include <stdint.h>
#include "architecture.h"

int add_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags);
int add_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags);
int adc_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags);
int adc_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags);

int sub_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags);
int sub_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags);
int sbc_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags);
int sbc_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags);

int rsb_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint32_t imm, uint_fast8_t flags);
int rsb_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t imm, uint_fast8_t flags);

int mul(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm, uint_fast8_t flags);

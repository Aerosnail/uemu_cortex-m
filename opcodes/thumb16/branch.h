#pragma once

#include <stdint.h>

#include "architecture.h"

int bx(struct cpu *cpu, uint_fast8_t rm);
int blx_imm(struct cpu *cpu, int32_t imm);
int blx_reg(struct cpu *cpu, uint_fast8_t rm);

int b(struct cpu *cpu, int32_t imm);
int b_cond(struct cpu *cpu, enum condition cond, int32_t imm);

int bl(struct cpu *cpu, int32_t imm);

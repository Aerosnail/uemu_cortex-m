#include "branch.h"
#include "cpu.h"

int
bx(struct cpu *cpu, uint_fast8_t rm)
{
	const uint32_t target = cpu->regs.r[rm];
	int ret;

	if (target >= EXC_RETURN) {
		/* Special case: returning frome an exception */
		cpu->regs.r[REG_PC] = target;
		ret = cpu_exception_return(cpu);
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC];
	} else {
		cpu->regs.r[REG_NEXT_PC] = target;
		ret = 0;
	}

	return ret;
}

int
blx_imm(struct cpu *cpu, int32_t imm)
{
	cpu->regs.r[REG_LR] = cpu->regs.r[REG_NEXT_PC];
	cpu->regs.r[REG_NEXT_PC] = imm;
	return 0;
}

int
blx_reg(struct cpu *cpu, uint_fast8_t rm)
{
	return blx_imm(cpu, cpu->regs.r[rm]);
}

int
b(struct cpu *cpu, int32_t imm)
{
	cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + imm;
	return 0;
}

int
b_cond(struct cpu *cpu, enum condition cond, int32_t imm)
{
	if (condition_passed(cpu, cond)) {
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + imm;
	}
	return 0;
}


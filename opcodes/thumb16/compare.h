#pragma once

#include <stdint.h>
#include "architecture.h"

int cmp_imm(struct cpu *cpu, uint_fast8_t rn, uint32_t imm);
int cmp_reg(struct cpu *cpu, uint_fast8_t rn, uint_fast8_t rm);
int cmn_imm(struct cpu *cpu, uint_fast8_t rn, uint32_t imm);
int cmn_reg(struct cpu *cpu, uint_fast8_t rn, uint_fast8_t rm);
int tst_imm(struct cpu *cpu, uint_fast8_t rn, uint32_t imm);
int tst_reg(struct cpu *cpu, uint_fast8_t rn, uint_fast8_t rm);

int cbz(struct cpu *cpu, uint_fast8_t rn, uint32_t imm);
int cbnz(struct cpu *cpu, uint_fast8_t rn, uint32_t imm);

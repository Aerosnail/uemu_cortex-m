#include <memory.h>
#include <utils.h>

#include "load.h"

int
str_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	uint32_t data;

	addr = cpu->regs.r[rn] + imm;
	data = cpu->regs.r[rt];

	return mem_write(cpu->bus, addr, &data, sizeof(data));
}

int
str_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return str_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
strh_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	uint16_t data;

	addr = cpu->regs.r[rn] + imm;
	data = cpu->regs.r[rt] & 0xFFFF;

	return mem_write(cpu->bus, addr, &data, sizeof(data));
}

int
strh_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return strh_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
strb_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	uint8_t data;

	addr = cpu->regs.r[rn] + imm;
	data = cpu->regs.r[rt] & 0xFF;

	return mem_write(cpu->bus, addr, &data, sizeof(data));
}

int
strb_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return strb_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
stmia(struct cpu *cpu, uint_fast8_t rn, uint_fast16_t regmask)
{
	uint32_t addr;
	size_t i;
	int ret = 0;

	addr = cpu->regs.r[rn];

	for (i=0; i<LEN(cpu->regs.r); i++) {
		if (regmask & (1 << i)) {
			ret |= mem_write(cpu->bus, addr, &cpu->regs.r[i], 4);
			addr += 4;
		}
	}

	cpu->regs.r[rn] = addr;

	return ret;
}

int
push(struct cpu *cpu, uint_fast16_t regmask)
{
	uint32_t addr;
	int i;
	int ret = 0;

	addr = cpu->regs.r[REG_SP];

	/* Reversing the order so that the SP can be incrementally updated, without
	 * having to count the number of bits set in regmask before writing */
	for (i=LEN(cpu->regs.r) - 1; i>=0; i--) {
		if (regmask & (1 << i)) {
			addr -= 4;
			ret |= mem_write(cpu->bus, addr, &cpu->regs.r[i], sizeof(cpu->regs.r[i]));
		}
	}

	cpu->regs.r[REG_SP] = addr;

	return ret;
}

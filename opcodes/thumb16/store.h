#pragma once

#include <stdint.h>
#include "architecture.h"

int str_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int str_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);
int strh_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int strh_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);
int strb_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int strb_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);

int stmia(struct cpu *cpu, uint_fast8_t rn, uint_fast16_t regmask);
int push(struct cpu *cpu, uint_fast16_t regmask);

#pragma once

#include <stdint.h>
#include "architecture.h"

int ldr_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int ldr_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);
int ldrh_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int ldrh_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);
int ldrb_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int ldrb_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);

int ldrsb_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int ldrsb_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);
int ldrsh_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm);
int ldrsh_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm);

int ldmia(struct cpu *cpu, uint_fast8_t rn, uint_fast16_t regmask);
int pop(struct cpu *cpu, uint_fast16_t regmask);

int ldr_lit(struct cpu *cpu, uint_fast8_t rt, uint32_t imm);

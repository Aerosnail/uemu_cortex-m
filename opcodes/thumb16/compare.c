#include "compare.h"

int
cmp_imm(struct cpu *cpu, uint_fast8_t rn, uint32_t imm)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];

	result = op1 - imm;

	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	cpu->regs.apsr.c = op1 >= imm;
	cpu->regs.apsr.v = ((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);

	return 0;
}

int
cmp_reg(struct cpu *cpu, uint_fast8_t rn, uint_fast8_t rm)
{
	return cmp_imm(cpu, rn, cpu->regs.r[rm]);
}

int
cmn_imm(struct cpu *cpu, uint_fast8_t rn, uint32_t imm)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];

	result = op1 + imm;

	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	cpu->regs.apsr.c = result < op1 || result < imm;
	cpu->regs.apsr.v = !((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);

	return 0;
}

int
cmn_reg(struct cpu *cpu, uint_fast8_t rn, uint_fast8_t rm)
{
	return cmn_imm(cpu, rn, cpu->regs.r[rm]);
}

int
tst_imm(struct cpu *cpu, uint_fast8_t rn, uint32_t imm)
{
	uint32_t result;
	uint32_t op1 = cpu->regs.r[rn];

	result = op1 & imm;

	cpu->regs.apsr.n = result >> 31;
	cpu->regs.apsr.z = result == 0;
	cpu->regs.apsr.c = result < op1 || result < imm;
	cpu->regs.apsr.v = !((op1 ^ imm) >> 31) && ((result ^ op1) >> 31);

	return 0;
}

int
tst_reg(struct cpu *cpu, uint_fast8_t rn, uint_fast8_t rm)
{
	return tst_imm(cpu, rn, cpu->regs.r[rm]);
}

int
cbz(struct cpu *cpu, uint_fast8_t rn, uint32_t imm)
{
	if (cpu->regs.r[rn] == 0) {
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + imm;
	}

	return 0;
}

int
cbnz(struct cpu *cpu, uint_fast8_t rn, uint32_t imm)
{
	if (cpu->regs.r[rn] != 0) {
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + imm;
	}

	return 0;
}

#include <memory.h>
#include <utils.h>

#include "cpu.h"
#include "load.h"

int
ldr_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	uint32_t data;
	int ret;

	rt += rt == REG_PC;

	addr = cpu->regs.r[rn] + imm;
	ret = mem_read(cpu->bus, addr, &data, sizeof(data));

	cpu->regs.r[rt] = data;

	if (!ret && cpu->regs.r[REG_NEXT_PC] >= EXC_RETURN) {
		cpu->regs.r[REG_PC] = cpu->regs.r[REG_NEXT_PC];
		ret = cpu_exception_return(cpu);
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC];
	}

	return ret;
}

int
ldr_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return ldr_imm(cpu, rt, rn, cpu->regs.r[rm]);
}


int
ldrh_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	uint16_t data;
	int ret;

	rt += rt == REG_PC;

	addr = cpu->regs.r[rn] + imm;
	ret = mem_read(cpu->bus, addr, &data, sizeof(data));

	cpu->regs.r[rt] = data;

	return ret;
}

int
ldrh_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return ldrh_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
ldrb_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	uint8_t data;
	int ret;

	rt += rt == REG_PC;

	addr = cpu->regs.r[rn] + imm;
	ret = mem_read(cpu->bus, addr, &data, sizeof(data));

	cpu->regs.r[rt] = data;

	return ret;
}

int
ldrb_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return ldrb_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
ldrsb_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	int8_t data;
	int ret;

	rt += rt == REG_PC;

	addr = cpu->regs.r[rn] + imm;
	ret = mem_read(cpu->bus, addr, &data, sizeof(data));

	cpu->regs.r[rt] = (int32_t)data;

	return ret;
}

int
ldrsb_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return ldrsb_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
ldrsh_imm(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint32_t imm)
{
	uint32_t addr;
	int16_t data;
	int ret;

	rt += rt == REG_PC;

	addr = cpu->regs.r[rn] + imm;
	ret = mem_read(cpu->bus, addr, &data, sizeof(data));

	cpu->regs.r[rt] = (int32_t)data;

	return ret;
}

int
ldrsh_reg(struct cpu *cpu, uint_fast8_t rt, uint_fast8_t rn, uint_fast8_t rm)
{
	return ldrsh_imm(cpu, rt, rn, cpu->regs.r[rm]);
}

int
ldmia(struct cpu *cpu, uint_fast8_t rn, uint_fast16_t regmask)
{
	uint32_t addr;
	size_t i;
	int ret = 0;

	addr = cpu->regs.r[rn];

	for (i=0; i<LEN(cpu->regs.r); i++) {
		if (regmask & (1 << i)) {
			i += i == REG_PC;
			ret |= mem_read(cpu->bus, addr, &cpu->regs.r[i], 4);
			addr += 4;
		}
	}

	if (!(regmask & 1 << rn)) cpu->regs.r[rn] = addr;

	if (!ret && cpu->regs.r[REG_NEXT_PC] >= EXC_RETURN) {
		cpu->regs.r[REG_PC] = cpu->regs.r[REG_NEXT_PC];
		ret = cpu_exception_return(cpu);
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC];
	}

	return ret;
}

int
pop(struct cpu *cpu, uint_fast16_t regmask)
{
	int ret;
	return ldmia(cpu, REG_SP, regmask);

	return ret;
}

int
ldr_lit(struct cpu *cpu, uint_fast8_t rt, uint32_t imm)
{
	uint32_t addr;
	int ret;

	rt += rt == REG_PC;

	addr = (cpu->regs.r[REG_PC] & ~0x3) + imm;
	ret = mem_read(cpu->bus, addr, &cpu->regs.r[rt], 4);

	return ret;
}

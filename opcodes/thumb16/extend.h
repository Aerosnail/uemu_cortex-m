#pragma once

#include <stdint.h>

#include "architecture.h"

int sxth(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);
int sxtb(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);
int uxth(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);
int uxtb(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rm);

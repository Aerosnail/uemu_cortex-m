#pragma once

#include <stdint.h>

#include "architecture.h"

int lsl_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift);
int lsl_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);
int lsr_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift);
int lsr_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);
int asr_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift);
int asr_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);
int ror_imm(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t shift);
int ror_reg(struct cpu *cpu, uint_fast8_t rd, uint_fast8_t rn, uint_fast8_t rm);

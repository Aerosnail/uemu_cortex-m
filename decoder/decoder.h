#pragma once

#include <stdint.h>

#include "architecture.h"

int execute(struct cpu *cpu, uint32_t instr);

#include <errno.h>

#include "thumb32.h"

#include "opcodes/thumb16/branch.h"

static int execute_ldmstm(struct cpu *cpu, uint32_t instr);
static int execute_ldtb(struct cpu *cpu, uint32_t instr);
static int execute_dshift(struct cpu *cpu, uint32_t instr);
static int execute_cpsimdfp(struct cpu *cpu, uint32_t instr);
static int execute_dmodimm(struct cpu *cpu, uint32_t instr);
static int execute_miscctrl(struct cpu *cpu, uint32_t instr);
static int execute_dimm(struct cpu *cpu, uint32_t instr);
static int execute_blblx(struct cpu *cpu, uint32_t instr);

int
execute_t32(struct cpu *cpu, uint32_t instr)
{
	uint_fast8_t op1, op2, op;

	op1 = (instr >> (16+11)) & 0x3;
	op2 = (instr >> (16+4))  & 0x7F;
	op  = (instr >> 15)      & 0x1;


	switch (op1) {
	case 0x01:
		switch (op2 & 0x64) {
		case 0x00:  /* Load/store multiple */
			return execute_ldmstm(cpu, instr);

		case 0x04:  /* Load/store dual, load/store exclusive, table branch */
			return execute_ldtb(cpu, instr);

		case 0x20:
		case 0x24:
			/* Data-processing (shifted register) */
			return execute_dshift(cpu, instr);
		case 0x40: case 0x44:
		case 0x60: case 0x64:
			/* Coprocessor, advanced SIMD, floating point */
			return execute_cpsimdfp(cpu, instr);
		}
		break;

	case 0x02:
		switch ((op2 & 0x20) | op) {
		case 0x00:  /* Data processing (modified immediate) */
			return execute_dmodimm(cpu, instr);

		case 0x01:
		case 0x21:
			/* Branches and miscellaneous control */
			return execute_miscctrl(cpu, instr);

		case 0x20:  /* Data-processing (plain binary immedaite) */
			return execute_dimm(cpu, instr);

		}
		break;
	case 0x03:
		break;
	}

	return ENOSYS;
}

/* Static functions {{{ */
static int
execute_ldmstm(struct cpu *cpu, uint32_t instr)
{
	return ENOSYS;
}

static int
execute_ldtb(struct cpu *cpu, uint32_t instr)
{
	return ENOSYS;
}

static int
execute_dshift(struct cpu *cpu, uint32_t instr)
{
	return ENOSYS;
}

static int
execute_cpsimdfp(struct cpu *cpu, uint32_t instr)
{
	return ENOSYS;
}

static int
execute_dmodimm(struct cpu *cpu, uint32_t instr)
{
	return ENOSYS;
}

static int
execute_miscctrl(struct cpu *cpu, uint32_t instr)
{
	uint_fast8_t op1;

	op1 = (instr >> 12) & 0x7;

	switch (op1) {
	case 0x04: case 0x05:
		/* Branch with link and exchange */
	case 0x06: case 0x07:
		/* Branch with link */
		return execute_blblx(cpu, instr);

	default:
		break;
	}

	return ENOSYS;
}

static int
execute_dimm(struct cpu *cpu, uint32_t instr)
{
	return ENOSYS;
}

static int
execute_blblx(struct cpu *cpu, uint32_t instr)
{
	uint_fast32_t imm10, imm11;
	uint_fast32_t s, j1, j2;
	int32_t imm32;

	imm11 = instr & 0x000007FF;
	imm10 = instr & 0x03FF0000;
	s     = instr & 0x04000000;
	j1    = instr & 0x00002000;
	j2    = instr & 0x00000800;

	imm32  = (int32_t)(s << 5) >> 9 ^ 0x00c00000;
	imm32 ^= j1 << 10 | j2 << 11;
	imm32 |= (imm11 | imm10 >> 5) << 1;

	if (instr & (1 << 12)) {
		/* Branch with link */
		return bl(cpu, imm32);
	} else {
		/* Branch with link and exchange */
		return blx_imm(cpu, imm32);
	}
}
/* }}} */

#include "thumb16.h"

#include "opcodes/thumb16/arith.h"
#include "opcodes/thumb16/branch.h"
#include "opcodes/thumb16/compare.h"
#include "opcodes/thumb16/extend.h"
#include "opcodes/thumb16/load.h"
#include "opcodes/thumb16/logic.h"
#include "opcodes/thumb16/misc.h"
#include "opcodes/thumb16/move.h"
#include "opcodes/thumb16/reverse.h"
#include "opcodes/thumb16/shift.h"
#include "opcodes/thumb16/store.h"

static int execute_shaddsubmovcmp(struct cpu *cpu, uint16_t instr);
static int execute_dataproc(struct cpu *cpu, uint16_t instr);
static int execute_ldr(struct cpu *cpu, uint16_t instr);
static int execute_special(struct cpu *cpu, uint16_t instr);
static int execute_ldrstr(struct cpu *cpu, uint16_t instr);
static int execute_adrsprel(struct cpu *cpu, uint16_t instr);
static int execute_misc(struct cpu *cpu, uint16_t instr);
static int execute_stmldm(struct cpu *cpu, uint16_t instr);
static int execute_cbsvc(struct cpu *cpu, uint16_t instr);
static int execute_branch(struct cpu *cpu, uint16_t instr);

int
execute_t16(struct cpu *cpu, uint16_t instr)
{
	/* oooooo xxxxxxxxxx */
	uint_fast16_t sub;

	sub = instr >> 10;
	switch (sub) {
	case 0x00: case 0x01: case 0x02: case 0x03:
	case 0x04: case 0x05: case 0x06: case 0x07:
	case 0x08: case 0x09: case 0x0A: case 0x0B:
	case 0x0C: case 0x0D: case 0x0E: case 0x0F:
		/* Shift (immediate), add, sub, move, compare */
		return execute_shaddsubmovcmp(cpu, instr);

	case 0x10: /* Data processing */
		return execute_dataproc(cpu, instr);

	case 0x11: /* Special data instructions */
		return execute_special(cpu, instr);

	case 0x12:
	case 0x13:
		/* Load from literal pool */
		return execute_ldr(cpu, instr);

	case 0x14: case 0x15: case 0x16: case 0x17:
	case 0x18: case 0x19: case 0x1a: case 0x1b:
	case 0x1c: case 0x1d: case 0x1e: case 0x1f:
	case 0x20: case 0x21: case 0x22: case 0x23:
	case 0x24: case 0x25: case 0x26: case 0x27:
		/* Load/store single data item */
		return execute_ldrstr(cpu, instr);

	case 0x28:
	case 0x29:
	case 0x2a:
	case 0x2b:
		/* Generate relative address */
		return execute_adrsprel(cpu, instr);

	case 0x2c: case 0x2d:
	case 0x2e: case 0x2f:
		/* Miscellaneous 16-bit instructions */
		return execute_misc(cpu, instr);

	case 0x30: case 0x31:
	case 0x32: case 0x33:
		/* Load/store multiple */
		return execute_stmldm(cpu, instr);

	case 0x34: case 0x35:
	case 0x36: case 0x37:
		/* Conditional branch and supervisor call */
		return execute_cbsvc(cpu, instr);

	case 0x38:
	case 0x39:
		/* Unconditional branch */
		return execute_branch(cpu, instr);
	}

	return 1;
}

/* Static functions {{{ */
static int
execute_shaddsubmovcmp(struct cpu *cpu, uint16_t instr)
{
	/* 00 aaa iiiii  yyy zzz */
	/* 00 aaa bb xxx yyy zzz */
	/* 00 aaa rrr   iiiiiiii */
	uint_fast8_t imm3, imm5, imm8, rx, ry, rz, rr, subop;
	rz = instr & 0x07;
	ry = (instr >> 3) & 0x07;
	rx = imm3 = (instr >> 6) & 0x07;
	subop = (instr >> 9) & 0x3;
	rr = (instr >> 8) & 0x7;
	imm5 = (instr >> 6) & 0x1F;
	imm8 = instr & 0xFF;

	switch ((instr >> 11) & 0x7) {
	case 0x00:  /* LSL (imm) */
		return lsl_imm(cpu, rz, ry, imm5);

	case 0x01:  /* LSR (imm) */
		return lsr_imm(cpu, rz, ry, imm5);

	case 0x02:  /* ASR (imm) */
		return asr_imm(cpu, rz, ry, imm5);

	case 0x03:  /* ADDSUB (imm/reg) */
		switch (subop) {
		case 0x00:  /* ADD (reg) */
			return add_reg(cpu, rz, ry, rx, 1);

		case 0x01:  /* SUB (reg) */
			return sub_reg(cpu, rz, ry, rx, 1);

		case 0x02:  /* ADD (imm) */
			return add_imm(cpu, rz, ry, imm3, 1);

		case 0x03:  /* SUB (imm) */
			return sub_imm(cpu, rz, ry, imm3, 1);
		}
		break;

	case 0x04:  /* MOV (imm) */
		return mov_imm(cpu, rr, imm8, 1);

	case 0x05:  /* CMP (imm) */
		return cmp_imm(cpu, rr, imm8);

	case 0x06:  /* ADD (imm) */
		return add_imm(cpu, rr, rr, imm8, 1);

	case 0x07:  /* SUB (imm) */
		return sub_imm(cpu, rr, rr, imm8, 1);
	}

	return 1;
}

static int
execute_dataproc(struct cpu *cpu, uint16_t instr)
{
	uint_fast8_t subfcn;
	uint_fast8_t rx, ry;

	ry = instr & 0x7;
	rx = (instr >> 3) & 0x7;
	subfcn = (instr >> 6) & 0xF;

	switch (subfcn) {
	case 0x00:
		return and_reg(cpu, ry, ry, rx);
	case 0x01:
		return xor_reg(cpu, ry, ry, rx);
	case 0x02:
		return lsl_reg(cpu, ry, ry, rx);
	case 0x03:
		return lsr_reg(cpu, ry, ry, rx);
	case 0x04:
		return asr_reg(cpu, ry, ry, rx);
	case 0x05:
		return adc_reg(cpu, ry, ry, rx, 1);
	case 0x06:
		return sbc_reg(cpu, ry, ry, rx, 1);
	case 0x07:
		return ror_reg(cpu, ry, ry, rx);
	case 0x08:
		return tst_reg(cpu, ry, rx);
	case 0x09:
		return rsb_imm(cpu, ry, rx, 0, 1);
	case 0x0a:
		return cmp_reg(cpu, ry, rx);
	case 0x0b:
		return cmn_reg(cpu, ry, rx);
	case 0x0c:
		return orr_reg(cpu, ry, ry, rx);
	case 0x0d:
		return mul(cpu, ry, rx, ry, 1);
	case 0x0e:
		return bic_reg(cpu, ry, ry, rx);
	case 0x0f:
		return mvn_reg(cpu, ry, rx, 1);
	default:
		break;
	}

	/* unreached */
	return 1;
}

static int
execute_special(struct cpu *cpu, uint16_t instr)
{
	uint_fast8_t subfcn;
	uint_fast8_t rx, ry;

	ry = (instr & 0x7) | (instr & (1 << 7)) >> 4;
	rx = (instr >> 3) & 0xF;
	subfcn = (instr >> 7) & 0x7;

	switch (subfcn) {
	case 0x00:
	case 0x01:
		return add_reg(cpu, ry, rx, ry, 0);

	case 0x02:
	case 0x03:
		return cmp_reg(cpu, ry, rx);

	case 0x04:
	case 0x05:
		return mov_reg(cpu, ry, rx, 0);

	case 0x06:
		return bx(cpu, rx);

	case 0x07:
		return blx_reg(cpu, rx);

	default:
		break;
	}

	/* Unreached */
	return 1;
}

static int
execute_ldr(struct cpu *cpu, uint16_t instr)
{
	uint_fast16_t imm8;
	uint_fast8_t rx;

	imm8 = (instr & 0xFF) << 2;
	rx = (instr >> 8) & 0x7;

	return ldr_lit(cpu, rx, imm8);
}


static int
execute_ldrstr(struct cpu *cpu, uint16_t instr)
{
	uint_fast8_t subop1, subop2;
	uint_fast8_t rx, ry, rz, rt;
	uint_fast16_t imm5, imm8;

	imm8 = instr & 0xFF;
	rz = instr & 0x7;
	ry = (instr >> 3) & 0x7;
	rx = (instr >> 6) & 0x7;
	rt = (instr >> 8) & 0x7;
	imm5 = (instr >> 6) & 0x1F;

	subop2 = (instr >> 9) & 0x7;
	subop1 = (instr >> 12) & 0xF;

	switch (subop1) {
	case 0x05:
		switch (subop2) {
		case 0x00:
			return str_reg(cpu, rz, ry, rx);
		case 0x01:
			return strh_reg(cpu, rz, ry, rx);
		case 0x02:
			return strb_reg(cpu, rz, ry, rx);
		case 0x03:
			return ldrsb_reg(cpu, rz, ry, rx);
		case 0x04:
			return ldr_reg(cpu, rz, ry, rx);
		case 0x05:
			return ldrh_reg(cpu, rz, ry, rx);
		case 0x06:
			return ldrb_reg(cpu, rz, ry, rx);
		case 0x07:
			return ldrsh_reg(cpu, rz, ry, rx);
		default:
			break;
		}
		break;

	case 0x06:
		if (subop2 >> 2) return ldr_imm(cpu, rz, ry, imm5 << 2);
		return str_imm(cpu, rz, ry, imm5 << 2);

	case 0x07:
		if (subop2 >> 2) return ldrb_imm(cpu, rz, ry, imm5);
		return strb_imm(cpu, rz, ry, imm5);

	case 0x08:
		if (subop2 >> 2) return ldrh_imm(cpu, rz, ry, imm5 << 1);
		return strh_imm(cpu, rz, ry, imm5 << 1);

	case 0x09:
		if (subop2 >> 2) return ldr_imm(cpu, rt, REG_SP, imm8 << 2);
		return str_imm(cpu, rt, REG_SP, imm8 << 2);

	default:
		break;
	}

	return 1;
}

static int
execute_adrsprel(struct cpu *cpu, uint16_t instr)
{
	uint_fast16_t imm8;
	uint_fast8_t rx;

	imm8 = instr & 0xFF;
	rx = (instr >> 8) & 0x7;

	if (instr & (1 << 11)) return add_imm(cpu, rx, REG_SP, imm8 << 2, 0);
	return adr(cpu, rx, imm8 << 2);
}

static int
execute_misc(struct cpu *cpu, uint16_t instr)
{
	uint_fast8_t subop;
	uint_fast16_t imm7;
	uint_fast16_t imm6;
	uint_fast8_t rx, ry;
	uint_fast16_t regmask;

	subop = (instr >> 6) & 0x3F;
	imm7 = instr & 0x7F;
	ry = instr & 0x7;
	rx = (instr >> 3) & 0x7;
	imm6 = ((instr >> 3) & 0x1F) | (instr & (1 << 8)) >> 4;
	regmask = instr & 0xFF;

	switch (subop) {
	case 0x00:
	case 0x01:
		return add_imm(cpu, REG_SP, REG_SP, imm7 << 2, 0);

	case 0x02:
	case 0x03:
		return sub_imm(cpu, REG_SP, REG_SP, imm7 << 2, 0);

	case 0x04: case 0x05:
	case 0x0c: case 0x0d:
	case 0x24: case 0x25:
	case 0x2c: case 0x2d:
		return cbz(cpu, ry, imm6 << 1);

	case 0x06: case 0x07:
	case 0x0e: case 0x0f:
	case 0x26: case 0x27:
	case 0x2e: case 0x2f:
		return cbnz(cpu, ry, imm6 << 1);

	case 0x08:
		return sxth(cpu, ry, rx);

	case 0x09:
		return sxtb(cpu, ry, rx);

	case 0x0a:
		return uxth(cpu, ry, rx);

	case 0x0b:
		return uxtb(cpu, ry, rx);

	case 0x10: case 0x11:
	case 0x12: case 0x13:
		return push(cpu, regmask);

	case 0x14: case 0x15:
	case 0x16: case 0x17:
		return push(cpu, 1 << REG_LR | regmask);

	case 0x19:
		/* TODO CPS/SETEND */
		break;

	case 0x28:
		return rev(cpu, ry, rx);

	case 0x29:
		return rev16(cpu, ry, rx);

	case 0x2b:
		return revsh(cpu, ry, rx);

	case 0x30: case 0x31:
	case 0x32: case 0x33:
		return pop(cpu, regmask);

	case 0x34: case 0x35:
	case 0x36: case 0x37:
		return pop(cpu, 1 << REG_PC | regmask);

	case 0x38: case 0x39:
	case 0x3a: case 0x3b:
		/* TODO BKPT */
		break;

	case 0x3c: case 0x3d:
	case 0x3e: case 0x3f:
		/* TODO if-then/hints */
		break;
	}

	return 1;
}

static int
execute_stmldm(struct cpu *cpu, uint16_t instr)
{
	uint_fast8_t regmask;
	uint_fast8_t rn;

	regmask = instr & 0xFF;
	rn = (instr >> 8) & 0x7;

	if (instr & (1 << 11)) return ldmia(cpu, rn, regmask);
	return stmia(cpu, rn, regmask);
}

static int
execute_cbsvc(struct cpu *cpu, uint16_t instr)
{
	uint_fast8_t condition;
	int32_t imm8;

	imm8 = ((int32_t)(instr & 0xFF)) << 24 >> 24;
	condition = (instr >> 8) & 0xF;

	if (condition == 0xF) {
		/* TODO SVC */
		return 1;
	}


	return b_cond(cpu, condition, imm8 << 1);
}

static int
execute_branch(struct cpu *cpu, uint16_t instr)
{
	int32_t imm11;

	imm11 = ((int32_t)(instr & 0x7FF)) << 21 >> 21;

	return b(cpu, imm11 << 1);
}

/* }}} */

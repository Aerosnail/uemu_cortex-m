#pragma once

#include <stdint.h>

#include "architecture.h"

int execute_t32(struct cpu *cpu, uint32_t instr);

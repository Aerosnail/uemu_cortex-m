#pragma once

#include <stdint.h>

#include "architecture.h"

int execute_t16(struct cpu *cpu, uint16_t instr);

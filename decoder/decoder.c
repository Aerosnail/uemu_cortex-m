#include "decoder.h"

#include "thumb16.h"
#include "thumb32.h"

int
execute(struct cpu *cpu, uint32_t instr)
{
	uint_fast8_t size;
	int ret = 0;

	size = (instr >> 11) & 0x1F;

	/* Due to some compat madness, REG_PC is cur_instr + 4 */
	if (size > 0x1C) {
		/* 32 bit instruction */
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + 4;
		cpu->regs.r[REG_PC] = cpu->regs.r[REG_PC] + 4;

		instr = (instr << 16) | (instr >> 16);

		ret = execute_t32(cpu, instr);
	} else {
		cpu->regs.r[REG_NEXT_PC] = cpu->regs.r[REG_PC] + 2;
		cpu->regs.r[REG_PC] = cpu->regs.r[REG_PC] + 4;

		ret = execute_t16(cpu, instr & 0xFFFF);
	}

	cpu->regs.r[REG_PC] = cpu->regs.r[REG_NEXT_PC];

	return ret;
}

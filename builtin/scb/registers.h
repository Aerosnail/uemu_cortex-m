#pragma once

#include <utils.h>

REG_DEF(cpuid_implementer, 0xFF, 24)
REG_DEF(cpuid_variant, 0xF, 20)
REG_DEF(cpuid_constant, 0xF, 16)
REG_DEF(cpuid_part_no, 0xFFF, 4)
REG_DEF(cpuid_revision, 0xF, 0)

REG_DEF(icsr_nmipendset, 0x1, 31)
REG_DEF(icsr_pendsvset, 0x1, 28)
REG_DEF(icsr_pendsvclr, 0x1, 27)
REG_DEF(icsr_pendstset, 0x1, 26)
REG_DEF(icsr_pendstclr, 0x1, 25)
REG_DEF(icsr_isrpending, 0x1, 22)
REG_DEF(icsr_vectpending, 0x7F, 12)
REG_DEF(icsr_rettobase, 0x1, 11)
REG_DEF(icsr_vectactive, 0x1FF, 12)

REG_DEF(vtor_tbloff, 0x1FFFFFF, 7)

REG_DEF(aircr_vectkey, 0xFFFF, 16)
REG_DEF(aircr_endianness, 0x1, 15)
REG_DEF(aircr_prigroup, 0x7, 8)
REG_DEF(aircr_sysresetreq, 0x1, 2)
REG_DEF(aircr_vectclractive, 0x1, 1)
REG_DEF(aircr_vectreset, 0x1, 0)

REG_DEF(scr_sevonpend, 0x1, 4)
REG_DEF(scr_sleepdeep, 0x1, 2)
REG_DEF(scr_sleeponexit, 0x1, 1)

REG_DEF(ccr_stkalign, 0x1, 9)
REG_DEF(ccr_bfhfnmign, 0x1, 8)
REG_DEF(ccr_div_0_trp, 0x1, 4)
REG_DEF(ccr_unalign_trp, 0x1, 3)
REG_DEF(ccr_usersetmpend, 0x1, 1)
REG_DEF(ccr_nonbasethrena, 0x1, 0)

REG_DEF(shpr1_pri_7, 0xFF, 24)
REG_DEF(shpr1_pri_6, 0xFF, 16)
REG_DEF(shpr1_pri_5, 0xFF, 8)
REG_DEF(shpr1_pri_4, 0xFF, 0)

REG_DEF(shpr2_pri_11, 0xFF, 24)

REG_DEF(shpr3_pri_15, 0xFF, 24)
REG_DEF(shpr3_pri_14, 0xFF, 16)

REG_DEF(shcsr_usgfaultena, 0x1, 18)
REG_DEF(shcsr_busfaultena, 0x1, 17)
REG_DEF(shcsr_memfaultena, 0x1, 16)
REG_DEF(shcsr_svcallpended, 0x1, 15)
REG_DEF(shcsr_busfaultpended, 0x1, 14)
REG_DEF(shcsr_memfaultpended, 0x1, 13)
REG_DEF(shcsr_usgfaultpended, 0x1, 12)
REG_DEF(shcsr_systickact, 0x1, 11)
REG_DEF(shcsr_pendsvact, 0x1, 10)
REG_DEF(shcsr_monitoract, 0x1, 8)
REG_DEF(shcsr_svcallact, 0x1, 7)
REG_DEF(shcsr_esgfaultact, 0x1, 3)
REG_DEF(shcsr_busfaultact, 0x1, 1)
REG_DEF(shcsr_memfaultact, 0x1, 0)


REG_DEF(cfsr_mmfsr, 0xFF, 0)
	REG_DEF(mmfsr_mmarvalid, 0x1, 7)
	REG_DEF(mmfsr_mstkerr, 0x1, 4)
	REG_DEF(mmfsr_munstkerr, 0x1, 3)
	REG_DEF(mmfsr_daccviol, 0x1, 1)
	REG_DEF(mmfsr_iaccviol, 0x1, 0)
REG_DEF(cfsr_bfsr, 0xFF, 8)
	REG_DEF(bfsr_bfarvalid, 0x1, 7)
	REG_DEF(bfsr_stkerr, 0x1, 4)
	REG_DEF(bfsr_unstkerr, 0x1, 3)
	REG_DEF(bfsr_impreciserr, 0x1, 2)
	REG_DEF(bfsr_preciserr, 0x1, 1)
	REG_DEF(bfsr_ibuserr, 0x1, 0)
REG_DEF(cfsr_ufsr, 0xFFFF, 16)
	REG_DEF(ufsr_divbyzero, 0x1, 9)
	REG_DEF(ufsr_unaligned, 0x1, 8)
	REG_DEF(ufsr_nocp, 0x1, 3)
	REG_DEF(ufsr_invpc, 0x1, 2)
	REG_DEF(ufsr_invstate, 0x1, 1)
	REG_DEF(ufsr_undefinstr, 0x1, 0)

REG_DEF(hfsr_forced, 0x1, 30)
REG_DEF(hfsr_vecttbl, 0x1, 1)


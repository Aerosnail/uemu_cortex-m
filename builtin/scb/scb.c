#include <errno.h>
#include <string.h>

#include <memory.h>

#include "architecture.h"
#include "scb.h"
#include "registers.h"

#define CPUID_IMPLEMENTER_ARM 0x41
#define AIRCR_VECTKEY_UNLOCK 0x5FA

static int scb_read(void *ctx, uint64_t addr, void *data, size_t count);
static int scb_write(void *ctx, uint64_t addr, const void *data, size_t count);

enum regs {
	SCB_CPUID   = 0x00,
	SCB_ICSR    = 0x04,
	SCB_VTOR    = 0x08,
	SCB_AIRCR   = 0x0c,
	SCB_SCR     = 0x10,
	SCB_CCR     = 0x14,
	SCB_SHPR1   = 0x18,
	SCB_SHPR2   = 0x1c,
	SCB_SHPR3   = 0x20,
	SCB_SHCSR   = 0x24,
	SCB_CFSR    = 0x28,
	SCB_CFSR_MMFSR = SCB_CFSR,
	SCB_CFSR_BFSR  = SCB_CFSR + 1,
	SCB_CFSR_UFSR  = SCB_CFSR + 2,
	SCB_HFSR    = 0x2c,
	SCB_MMAR    = 0x34,
	SCB_BFAR    = 0x38,
	SCB_AFSR    = 0x3c,
};

struct scb_regs {
	uint32_t actlr;
	uint32_t cpuid;
	uint32_t vtor;
	uint32_t aircr;
	uint32_t scr;
	uint32_t ccr;
	uint32_t shcsr;
	uint32_t cfsr;
	uint32_t mmsr;
	uint32_t bfsr;
	uint32_t ufsr;
	uint32_t hfsr;
	uint32_t mmar;
	uint32_t bfar;
	uint32_t afsr;
};

struct scb {
	MemEntry *mem;
	MemBus *bus;
	InterruptManager *int_mgr;

	struct nvic *nvic;
	struct scb_regs regs;
};

struct scb*
scb_init(MemBus *bus, InterruptManager *int_mgr, uint32_t cpuid)
{
	struct scb *self;
	struct mem_handler handler = {
		.read = scb_read,
		.write = scb_write,
		.execute = scb_read,
	};

	self = calloc(1, sizeof(*self));
	if (!self) return NULL;

	self->int_mgr = int_mgr;
	self->bus = bus;

	self->regs.cpuid = cpuid;

	aircr_endianness_set(&self->regs.aircr, 0);

	handler.ctx = self;
	self->mem = mem_register(self->bus, &handler, SCB_START_ADDR, SCB_ADDR_LEN);

	return self;
}

int
scb_deinit(struct scb *self)
{
	int ret;

	ret = mem_unregister(self->bus, self->mem);
	free(self);

	return ret;
}

uint32_t
scb_get_vtor(struct scb *self)
{
	return self->regs.vtor;
}

static int
scb_read(void *ctx, uint64_t addr, void *data, size_t count)
{
	struct scb *self = (struct scb*)ctx;
	uint32_t *data_words = (uint32_t*)data;
	uint16_t *data_halfwords = (uint16_t*)data;
	uint8_t *data_bytes = (uint8_t*)data;
	uint32_t tmp;
	size_t i;

	for (i = 0; i < count; ) {
		switch (addr) {
		case SCB_CPUID:
			data_words[i / 4] = self->regs.cpuid;
			i += 4;
			break;

		case SCB_ICSR:
			tmp = 0;
			icsr_nmipendset_set(&tmp, int_get_pending(self->int_mgr, EXC_NMI));
			icsr_pendsvset_set(&tmp, int_get_pending(self->int_mgr, EXC_PENDSV));
			icsr_pendstset_set(&tmp, int_get_pending(self->int_mgr, EXC_SYSTICK));
			data_words[i / 4] = tmp;
			i += 4;
			break;

		case SCB_VTOR:
			data_words[i / 4] = self->regs.vtor;
			i += 4;
			break;

		case SCB_AIRCR:
			aircr_vectkey_set(&self->regs.aircr, 0xFA05);

			tmp = self->regs.aircr;
			aircr_sysresetreq_set(&tmp, 0);
			aircr_vectclractive_set(&tmp, 0);
			aircr_vectreset_set(&tmp, 0);

			data_words[i / 4] = tmp;
			i += 4;
			break;

		case SCB_SCR:
			data_words[i / 4] = self->regs.scr;
			i += 4;
			break;

		case SCB_CCR:
			data_words[i / 4] = self->regs.ccr;
			i += 4;
			break;

		case SCB_SHPR1:
			data_bytes[i++] = int_get_priority(self->int_mgr, 4);
			break;

		case SCB_SHPR1 + 1:
			data_bytes[i++] = int_get_priority(self->int_mgr, 5);
			break;

		case SCB_SHPR1 + 2:
			data_bytes[i++] = int_get_priority(self->int_mgr, 6);
			break;

		case SCB_SHPR1 + 3:
			data_bytes[i++] = int_get_priority(self->int_mgr, 7);
			break;

		case SCB_SHPR2:
		case SCB_SHPR2 + 1:
		case SCB_SHPR2 + 2:
			data_bytes[i++] = 0;
			break;

		case SCB_SHPR2 + 3:
			data_bytes[i++] = int_get_priority(self->int_mgr, 11);
			break;

		case SCB_SHPR3:
		case SCB_SHPR3 + 1:
			data_bytes[i++] = 0;
			break;

		case SCB_SHPR3 + 2:
			data_bytes[i++] = int_get_priority(self->int_mgr, 14);
			break;

		case SCB_SHPR3 + 3:
			data_bytes[i++] = int_get_priority(self->int_mgr, 15);
			break;

		case SCB_CFSR_MMFSR:
			data_bytes[i++] = cfsr_mmfsr_get(self->regs.cfsr);
			break;

		case SCB_CFSR_BFSR:
			data_bytes[i++] = cfsr_bfsr_get(self->regs.cfsr);
			break;

		case SCB_CFSR_UFSR:
			if (count < 2) return EFAULT;
			data_halfwords[i / 2] = cfsr_ufsr_get(self->regs.cfsr);
			i += 2;
			break;

		case SCB_SHCSR:
			data_words[i / 4] = self->regs.shcsr;
			i += 4;
			break;

		case SCB_HFSR:
			data_words[i / 4] = self->regs.hfsr;
			i += 4;
			break;

		case SCB_MMAR:
			data_words[i / 4] = self->regs.mmar;
			i += 4;
			break;

		case SCB_BFAR:
			data_words[i / 4] = self->regs.bfar;
			i += 4;
			break;

		case SCB_AFSR:
			data_words[i / 4] = self->regs.afsr;
			i += 4;
			break;

		default:
			return EFAULT;
		}
	}

	return 0;
}

static int
scb_write(void *ctx, uint64_t addr, const void *data, size_t count)
{
	const uint32_t *data_words = (const uint32_t*)data;
	const uint16_t *data_halfwords = (const uint16_t*)data;
	const uint8_t *data_bytes = (const uint8_t*)data;
	struct scb *self = (struct scb*)ctx;
	size_t i;

	for (i = 0; i < count; ) {
		switch (addr) {
		case SCB_CPUID:
			return EFAULT;

		case SCB_ICSR:
			if (icsr_nmipendset_get(data_words[i / 4])) {
				int_set_pending(self->int_mgr, EXC_NMI, 1);
			}

			if (icsr_pendsvclr_get(data_words[i / 4])) {
				int_set_pending(self->int_mgr, EXC_PENDSV, 0);
			} else if (icsr_pendsvset_get(data_words[i / 4])) {
				int_set_pending(self->int_mgr, EXC_PENDSV, 1);
			}

			if (icsr_pendstclr_get(data_words[i / 4])) {
				int_set_pending(self->int_mgr, EXC_SYSTICK, 0);
			} else if (icsr_pendstset_get(data_words[i / 4])) {
				int_set_pending(self->int_mgr, EXC_SYSTICK, 1);
			}
			i += 4;
			break;

		case SCB_VTOR:
			self->regs.vtor = data_words[i / 4];
			i += 4;
			break;

		case SCB_AIRCR:
			if (aircr_vectkey_get(data_words[i / 4]) != AIRCR_VECTKEY_UNLOCK) {
				break;
			}
			self->regs.aircr = data_words[i / 4];
			i += 4;

			aircr_endianness_set(&self->regs.aircr, 0);

			if (aircr_sysresetreq_get(self->regs.aircr)) {
				int_set_pending(self->int_mgr, EXC_RESET, 1);
			}
			break;

		case SCB_SCR:
			self->regs.scr = data_words[i / 4];
			i += 4;
			break;

		case SCB_CCR:
			self->regs.ccr = data_words[i / 4];
			i += 4;
			break;

		case SCB_SHPR1:
			int_set_priority(self->int_mgr, 4, data_bytes[i++]);
			break;

		case SCB_SHPR1 + 1:
			int_set_priority(self->int_mgr, 5, data_bytes[i++]);
			break;

		case SCB_SHPR1 + 2:
			int_set_priority(self->int_mgr, 6, data_bytes[i++]);
			break;

		case SCB_SHPR1 + 3:
			int_set_priority(self->int_mgr, 7, data_bytes[i++]);
			break;

		case SCB_SHPR2:
		case SCB_SHPR2 + 1:
		case SCB_SHPR2 + 2:
			i++;
			break;

		case SCB_SHPR2 + 3:
			int_set_priority(self->int_mgr, 11, data_bytes[i++]);
			break;

		case SCB_SHPR3:
		case SCB_SHPR3 + 1:
			i++;
			break;

		case SCB_SHPR3 + 2:
			int_set_priority(self->int_mgr, 14, data_bytes[i++]);
			break;

		case SCB_SHPR3 + 3:
			int_set_priority(self->int_mgr, 15, data_bytes[i++]);
			break;

		case SCB_CFSR_MMFSR:
			cfsr_mmfsr_clear(&self->regs.cfsr, data_bytes[i++]);
			break;

		case SCB_CFSR_BFSR:
			cfsr_bfsr_clear(&self->regs.cfsr, data_bytes[i++]);
			break;

		case SCB_CFSR_UFSR:
			if (count < 2) return EFAULT;
			cfsr_ufsr_clear(&self->regs.cfsr, data_halfwords[i / 2]);
			i += 2;
			break;

		case SCB_SHCSR:
			shcsr_usgfaultena_set(&self->regs.shcsr, shcsr_usgfaultena_get(data_words[i / 4]));
			shcsr_busfaultena_set(&self->regs.shcsr, shcsr_busfaultena_get(data_words[i / 4]));
			shcsr_memfaultena_set(&self->regs.shcsr, shcsr_memfaultena_get(data_words[i / 4]));
			i += 4;
			break;

		case SCB_HFSR:
			hfsr_forced_clear(&self->regs.hfsr, hfsr_forced_get(data_words[i / 4]));
			hfsr_vecttbl_clear(&self->regs.hfsr, hfsr_vecttbl_get(data_words[i / 4]));
			i += 4;
			break;

		case SCB_MMAR:
			self->regs.mmar &= ~data_words[i / 4];
			i += 4;
			break;

		case SCB_BFAR:
			self->regs.bfar &= ~data_words[i / 4];
			i += 4;
			break;

		case SCB_AFSR:
			self->regs.afsr &= ~data_words[i / 4];
			i += 4;
			break;

		default:
			return EFAULT;
		}
	}

	return 0;
}

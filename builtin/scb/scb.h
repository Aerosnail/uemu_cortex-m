#pragma once

#include <stdint.h>
#include <stdlib.h>

#include <interrupts.h>
#include <memory.h>

#include "../nvic/nvic.h"

#define SCB_START_ADDR  0xE000ED00
#define SCB_ADDR_LEN    0x100

struct scb;

struct scb* scb_init(MemBus *bus, InterruptManager *int_mgr, uint32_t cpuid);
int scb_deinit(struct scb *self);

uint32_t scb_get_vtor(struct scb *self);

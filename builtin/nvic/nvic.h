#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include <gpio.h>
#include <memory.h>
#include <interrupts.h>

#define NVIC_START_ADDR 0xE000E100
#define NVIC_ADDR_LEN   0x340
#define NVIC_INT_COUNT  240
#define NVIC_EXTI_COUNT 16

struct nvic;

struct nvic* nvic_init(MemBus *bus, InterruptManager *int_mgr, const char *name);
int nvic_deinit(struct nvic *self);

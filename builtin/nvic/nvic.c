#include <assert.h>
#include <errno.h>
#include <string.h>

#include <gpio.h>
#include <memory.h>
#include <interrupts.h>
#include <utils.h>

#include "architecture.h"
#include "nvic.h"


enum regs {
	OFF_NVIC_ISER = 0x0000,
	OFF_NVIC_ICER = 0x0080,
	OFF_NVIC_ISPR = 0x0100,
	OFF_NVIC_ICPR = 0x0180,
	OFF_NVIC_IABR = 0x0200,
	OFF_NVIC_IPR  = 0x0300,
	NVIC_STIR     = 0x0E00
};

struct exti_ctx {
	struct nvic *nvic;
	int irq;
};

struct nvic {
	InterruptManager *int_mgr;
	MemBus *bus;

	MemEntry *mem;

	GPIO *exti[NVIC_EXTI_COUNT];
	void *exti_ctx[NVIC_EXTI_COUNT];
};

static int nvic_handle_exti(void *self, int value);
static int nvic_read(void *ctx, uint64_t addr, void *v_data, size_t count);
static int nvic_write(void *ctx, uint64_t addr, const void *v_data, size_t count);

struct nvic*
nvic_init(MemBus *bus, InterruptManager *int_mgr, const char *name)
{
	struct nvic *self;
	struct exti_ctx *exti_ctx;
	size_t i;
	char gpio_name[16];
	struct mem_handler handler = {
		.read = nvic_read,
		.write = nvic_write,
		.execute = nvic_read,
	};

	self = calloc(1, sizeof(*self));
	if (!self) return NULL;

	self->bus = bus;
	self->int_mgr = int_mgr;

	for (i=0; i<LEN(self->exti); i++) {
		sprintf(gpio_name, ":%d", (int)i);

		exti_ctx = calloc(1, sizeof(*exti_ctx));
		exti_ctx->nvic = self;
		exti_ctx->irq = i;

		self->exti[i] = gpio_create_instance(name, gpio_name, nvic_handle_exti, exti_ctx);
		self->exti_ctx[i] = exti_ctx;
	}

	handler.ctx = self;
	self->mem = mem_register(self->bus, &handler, NVIC_START_ADDR, NVIC_ADDR_LEN);

	return self;
}

int
nvic_deinit(struct nvic *self)
{
	int ret;
	size_t i;

	ret = 0;

	for (i=0; i<LEN(self->exti); i++) {
		ret |= gpio_delete_instance(self->exti[i]);
		free(self->exti_ctx[i]);
	}

	ret |= mem_unregister(self->bus, self->mem);
	free(self);

	return ret;
}

static int
nvic_handle_exti(void *ctx, int value)
{
	struct exti_ctx *self = (struct exti_ctx*)ctx;

	if (value) {
		int_set_pending(self->nvic->int_mgr, self->irq, 1);
	}
	return 0;
}

/* Memory handlers {{{ */
static int
nvic_read(void *ctx, uint64_t addr, void *v_data, size_t len)
{
	struct nvic *self = (struct nvic*)ctx;
	uint8_t *data_bytes = (uint8_t*)v_data;
	uint32_t *data_words = (uint32_t*)v_data;

	size_t i, j;
	uint_fast32_t block, offset;
	uint32_t tmp;

	for (i = 0; i < len;) {
		block  = (addr + i) & 0xFF80;
		offset = (addr + i) & 0x007F;

		switch (block) {
		case OFF_NVIC_ISER:
		case OFF_NVIC_ICER:
			/* Interrupt set/clear enable: return enabled interrupts  */
			tmp = 0;
			for (j = 0; j < 8 * sizeof(*data_words); j++) {
				tmp = (tmp << 1) | int_get_masked(self->int_mgr,
				                                  EXC_INT_BASE + offset * 8 + j);
			}

			data_words[i / 4] = tmp;
			i += 4;
			break;

		case OFF_NVIC_ISPR:
		case OFF_NVIC_ICPR:
			/* Interrupt set/clear pending: return pending interrupts */
			tmp = 0;
			for (j = 0; j < 32; j++) {
				tmp = (tmp << 1) | int_get_pending(self->int_mgr,
				                                   EXC_INT_BASE + offset * 8 + j);
			}

			data_words[i / 4] = tmp;
			i += 4;
			break;

		case OFF_NVIC_IPR:
		case OFF_NVIC_IPR + 1:
			/* Interrupt priority: return priority */
			offset = (addr & 0xFF);
			data_bytes[i] = int_get_priority(self->int_mgr, EXC_INT_BASE + offset);
			i++;
			break;

		case OFF_NVIC_IABR:
			return ENOTSUP;

		default:
			return EFAULT;
		}
	}

	return 0;
}

static int
nvic_write(void *ctx, uint64_t addr, const void *v_data, size_t len)
{
	struct nvic *self = (struct nvic*)ctx;
	uint8_t *data_bytes = (uint8_t*)v_data;
	uint32_t *data_words = (uint32_t*)v_data;

	size_t i, j;
	uint_fast32_t block, offset;

	if (len != 4) return EFAULT;

	for (i = 0; i < len; ) {
		block  = (addr + i) & 0xFF80;
		offset = (addr + i) & 0x7F;

		switch (block) {
		case OFF_NVIC_ISER:
			/* Set enabled */
			for (j=0; j<32; j++) {
				if (data_words[i/4] & (1 << j)) {
					int_set_masked(self->int_mgr, EXC_INT_BASE + offset * 8 + j, 0);
				}
			}
			i += 4;
			break;

		case OFF_NVIC_ICER:
			/* Clear enabled */
			for (j=0; j<32; j++) {
				if (data_words[i/4] & (1 << j)) {
					int_set_masked(self->int_mgr, EXC_INT_BASE + offset * 8 + j, 1);
				}
			}
			i += 4;
			break;

		case OFF_NVIC_ISPR:
			/* Set pending */
			for (j=0; j<32; j++) {
				if (data_words[i/4] & (1 << j)) {
					int_set_pending(self->int_mgr, EXC_INT_BASE + offset * 8 + j, 1);
				}
			}
			i += 4;
			break;

		case OFF_NVIC_ICPR:
			/* Clear pending */
			for (j=0; j<32; j++) {
				if (data_words[i/4] & (1 << j)) {
					int_set_pending(self->int_mgr, EXC_INT_BASE + offset * 8 + j, 0);
				}
			}
			i += 4;
			break;

		case OFF_NVIC_IABR:
			return ENOTSUP;

		case OFF_NVIC_IPR:
		case OFF_NVIC_IPR + 1:
			offset = (addr & 0xFF);
			int_set_priority(self->int_mgr, offset, EXC_INT_BASE + data_bytes[i]);
			i++;
			break;


		default:
			switch (addr) {
			case NVIC_STIR:
				int_set_pending(self->int_mgr, EXC_INT_BASE + data_bytes[i], 1);
				break;

			default:
				return EFAULT;
			}
		}
	}

	return 0;
}
/* }}} */

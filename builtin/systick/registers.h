#pragma once

#include <utils.h>

REG_DEF(csr_countflag, 0x1, 16)
REG_DEF(csr_clksource, 0x1, 2)
REG_DEF(csr_tickint, 0x1, 1)
REG_DEF(csr_enable, 0x1, 0)

REG_DEF(rvr_reload, 0xFFFFFF, 0)

REG_DEF(cvr_current, 0xFFFFFF, 0)

REG_DEF(calib_noref, 0x1, 31)
REG_DEF(calib_skew, 0x1, 30)
REG_DEF(calib_tenms, 0xFFFFFF, 0)

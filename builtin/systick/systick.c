#include <errno.h>
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h>

#include <log/log.h>
#include <utils.h>

#include "architecture.h"
#include "registers.h"
#include "systick.h"

#define SYSTICK_RVR_FOR_10MS_TIMING 10000

static int systick_read(void *ctx, uint64_t addr, void *data, size_t count);
static int systick_write(void *ctx, uint64_t addr, const void *data, size_t count);
static void* intgen_thread(void *args);

enum regs {
	SYST_CSR    = 0x00,
	SYST_RVR    = 0x04,
	SYST_CVR    = 0x08,
	SYST_CALIB  = 0x0c,
};

struct systick_regs {
	uint32_t csr;
	uint32_t rvr;
	uint32_t cvr;
	uint32_t calib;
};

struct systick {
	MemBus *bus;
	InterruptManager *int_mgr;
	MemEntry *mem;

	struct systick_regs regs;

	struct timeval start_time, countflag_read_time;

	pthread_t tid;
	int thread_running;
};

struct systick*
systick_init(MemBus *bus, InterruptManager *int_mgr)
{

	struct systick *self = calloc(1, sizeof(*self));
	struct mem_handler handler = {
		.read = systick_read,
		.write = systick_write,
		.execute = systick_read,
	};

	if (!self) return NULL;

	self->bus = bus;
	self->int_mgr = int_mgr;
	self->thread_running = 0;

	handler.ctx = self;

	calib_noref_set(&self->regs.calib, 0);
	calib_skew_set(&self->regs.calib, 0);
	calib_tenms_set(&self->regs.calib, SYSTICK_RVR_FOR_10MS_TIMING);

	self->mem = mem_register(bus, &handler, SYSTICK_START_ADDR, SYSTICK_ADDR_LEN);

	return self;
}

int
systick_deinit(struct systick *self)
{
	int ret;

	if (self->tid && self->thread_running) {
		self->thread_running = 0;
		pthread_join(self->tid, NULL);
	}

	ret = mem_unregister(self->bus, self->mem);
	free(self);

	return ret;
}

/* Memory handlers {{{ */
static int
systick_read(void *ctx, uint64_t addr, void *vdata, size_t len)
{
	struct systick *self = (struct systick*)ctx;
	uint32_t *data_words = (uint32_t*)vdata;
	struct timeval now;
	uint64_t delta_t_us;
	uint32_t reload_val;

	if (len != 4) return EINVAL;

	switch (addr) {
	case SYST_CSR:
		if (csr_enable_get(self->regs.csr)) {
			gettimeofday(&now, NULL);
			delta_t_us = (now.tv_sec - self->start_time.tv_sec) * 1000000
					   + now.tv_usec - self->start_time.tv_usec;
			reload_val = rvr_reload_get(self->regs.rvr);

			csr_countflag_set(&self->regs.csr, delta_t_us > reload_val);
			self->countflag_read_time = now;
		}
		*data_words = self->regs.csr;

		/* Immediately reset flag since it was just read */
		csr_countflag_set(&self->regs.csr, 0);
		break;

	case SYST_RVR:
		*data_words = self->regs.rvr;
		break;

	case SYST_CVR:
		if (csr_enable_get(self->regs.csr)) {
			/* Update counter value and countflag */
			gettimeofday(&now, NULL);
			delta_t_us = (now.tv_sec - self->start_time.tv_sec) * 1000000
			           + now.tv_usec - self->start_time.tv_usec;
			reload_val = rvr_reload_get(self->regs.rvr);

			cvr_current_set(&self->regs.cvr, reload_val - (delta_t_us % reload_val));
		}

		*data_words = self->regs.cvr;
		break;

	case SYST_CALIB:
		*data_words = self->regs.calib;
		break;

	default:
		return EACCES;
	}

	return 0;
}

static int
systick_write(void *ctx, uint64_t addr, const void *vdata, size_t len)
{
	struct systick *self = (struct systick*)ctx;
	const uint32_t *data_words = (const uint32_t*)vdata;

	if (len != 4) return EINVAL;

	switch (addr) {
	case SYST_CSR:
		self->regs.csr = *data_words;
		self->thread_running = csr_tickint_get(self->regs.csr);

		if (self->thread_running && !self->tid) {
			/* Start generating periodic interrupts */
			pthread_create(&self->tid, NULL, intgen_thread, self);
		} else if (self->tid && !self->thread_running) {
			pthread_join(self->tid, NULL);
		}
		break;

	case SYST_RVR:
		rvr_reload_set(&self->regs.rvr, rvr_reload_get(*data_words));
		break;

	case SYST_CVR:
		self->regs.cvr = 0;
		csr_countflag_set(&self->regs.csr, 0);
		gettimeofday(&self->start_time, NULL);
		self->countflag_read_time = self->start_time;
		break;

	case SYST_CALIB:
	default:
		return EACCES;
	}

	return 0;

}

static void*
intgen_thread(void *args)
{
	struct systick *self = (struct systick*)args;

	while (self->thread_running) {
		usleep(rvr_reload_get(self->regs.rvr));
		int_set_pending(self->int_mgr, EXC_SYSTICK, 1);
	}

	return NULL;
}
/* }}} */

#pragma once

#include <stdint.h>
#include <stdlib.h>

#include <memory.h>
#include <interrupts.h>

#include "../nvic/nvic.h"

#define SYSTICK_START_ADDR 0xE000E010
#define SYSTICK_ADDR_LEN   0x100

struct systick;

struct systick* systick_init(MemBus *bus, InterruptManager *int_mgr);
int systick_deinit(struct systick *self);

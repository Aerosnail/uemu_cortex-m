#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <module.h>
#include <utils.h>

#include "commands.h"
#include "cpu.h"
#include "devicetree.h"

static const char *_compatible[] = {
	"arm,cortex-m0",
	"arm,cortex-m0plus",
	"arm,cortex-m0+",
	"arm,cortex-m1",
	NULL
};

__global
ModuleInfo __info = {
	.name = "cortex-m",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible,
};

__global int
__init(void)
{
	return 0;
}

__global int
__parse_dtb_prop(void **dst, const struct dtb_property *prop, find_phandle_t find_phandle, void *find_phandle_ctx)
{
	struct cortex_m_specs *specs;

	if (!*dst) *dst = calloc(1, sizeof(*specs));
	specs = (struct cortex_m_specs*)*dst;

	return parse_dtb_prop(specs, prop, find_phandle, find_phandle_ctx);
}

__global int
__create_instance(void **dst, ModuleSpecs *specs)
{
	struct cortex_m_specs *custom_specs = (struct cortex_m_specs*)specs->ctx;
	struct cpu *self;
	uint32_t cpuid = 0;
	int ret;

	if (custom_specs) {
		cpuid = custom_specs->cpuid;
		free(custom_specs);
	}

	ret = cpu_init(&self, specs->bus, specs->name, cpuid);
	if (ret) return ret;

	register_commands(specs->name, self);

	return 0;
}

__global int
__delete_instance(void *ctx)
{
	int ret = 0;
	struct cpu *self = (struct cpu*)ctx;

	ret |= unregister_commands(self->name);
	ret |= cpu_deinit(self);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}

#include <errno.h>
#include <string.h>

#include <dtb/node.h>

#include "devicetree.h"

enum props {
	PROP_CPUID = 0,
	PROP_COUNT
};

static const char *_prop_names[] = {
	[PROP_CPUID] = "arm,cpuid",
};

int
parse_dtb_prop(struct cortex_m_specs *dst, const struct dtb_property *prop,
                   find_phandle_t find_phandle, void *find_phandle_ctx)
{
	size_t i;

	(void)find_phandle;
	(void)find_phandle_ctx;

	for (i = 0; i < PROP_COUNT; i++) {
		if (!strcmp(prop->name, _prop_names[i])) {
			break;
		}
	}

	switch (i) {
	case PROP_CPUID:
		dtb_property_get_u32(&dst->cpuid, prop, 0, 1);
		break;

	default:
		return ENOKEY;
	}

	return 0;
}

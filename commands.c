#include <assert.h>
#include <command.h>
#include <inttypes.h>
#include <string.h>

#include <log/log.h>
#include <utils.h>

#include "commands.h"
#include "cpu.h"

typedef struct {
	const char *cmd;
	const char *help;
	int (*fcn)(int, const char*[], void*);
} command_t;

static int dumpstate(int argc, const char *argv[], void *ctx);
static int reset(int argc, const char *argv[], void *ctx);
static int step(int argc, const char *argv[], void *ctx);
static int halt(int argc, const char *argv[], void *ctx);
static int run(int argc, const char *argv[], void *ctx);
static int until(int argc, const char *argv[], void *ctx);
static int breakpoint(int argc, const char *argv[], void *ctx);

static void* thread_main(void *args);

static const command_t cmds[] = {
	{
		.cmd = "dumpstate",
		.help = "Show the internal state of the processor",
		.fcn = dumpstate,
	},
	{
		.cmd = "reset",
		.help = "Reset the processor",
		.fcn = reset,
	},
	{
		.cmd = "step",
		.help = "Step through the next instruction",
		.fcn = step,
	},
	{
		.cmd = "run",
		.help = "Start executing instructions",
		.fcn = run,
	},
	{
		.cmd = "until",
		.help = "Execute instructions until PC=value",
		.fcn = until,
	},
	{
		.cmd = "halt",
		.help = "Halt instruction execution",
		.fcn = halt
	},
	{
		.cmd = "break",
		.help = "Add a breakpoint when PC=value",
		.fcn = breakpoint
	},
};

int
register_commands(const char *name, struct cpu *ctx)
{
	int ret = 0;
	size_t i;

	for (i=0; i<LEN(cmds); i++) {
		ret |= cmd_register_module(name,
				cmds[i].cmd, cmds[i].help, cmds[i].fcn, ctx);
	}

	return ret;
}

int
unregister_commands(const char *name)
{
	return cmd_unregister_module(name, NULL);
}

/* Static functions {{{ */
static int
reset(int argc, const char *argv[], void *ctx)
{
	(void)argc;
	(void)argv;

	struct cpu *self = (struct cpu*)ctx;
	return cpu_reset(self);
}

static int
step(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	unsigned long stepcount = 1;
	int ret = 0;

	if (argc > 1) {
		stepcount = strtoul(argv[1], NULL, 10);
	}

	if (self->tid) {
		log_error("Cannot step while CPU is running");
		return 1;
	}

	for (; stepcount > 0; stepcount--) {
		ret |= cpu_step(self);
	}
	ret |= dumpstate(0, NULL, self);


	return ret;
}

static int
until(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	uint32_t pc;
	int ret = 0;

	if (argc < 2) {
		return 1;
	}

	pc = strtoul(argv[1], NULL, 0);

	if (self->tid) {
		log_error("Cannot step while CPU is running");
		return 1;
	}

	while (self->regs.r[REG_PC] != pc) {
		ret |= cpu_step(self);
		ret |= dumpstate(0, NULL, self);
	}

	return ret;
}

static int
breakpoint(int argc, const char *argv[], void *ctx)
{
	/* TODO implement */
	return 1;
}

static int
run(int argc, const char *argv[], void *ctx)
{
	(void)argc;
	(void)argv;

	struct cpu *self = (struct cpu*)ctx;

	if (self->thread_running || self->tid) {
		log_warn("CPU is already running");
		return 1;
	}

	self->thread_running = true;
	pthread_create(&self->tid, NULL, thread_main, ctx);

	if (!self->tid) {
		log_error("Failed to start CPU thread");
		self->thread_running = false;
		return 1;
	}

	return 0;
}

static int
halt(int argc, const char *argv[], void *ctx)
{
	(void)argc;
	(void)argv;
	int ret;

	struct cpu *self = (struct cpu*)ctx;

	if (!self->tid) {
		log_error("Not running");
		return 1;
	}

	self->thread_running = false;
	ret = pthread_join(self->tid, NULL);

	self->tid = 0;

	return ret;
}

static int
dumpstate(int argc, const char *argv[], void *ctx)
{
	(void)argc;
	(void)argv;
	struct cpu *self = (struct cpu*)ctx;
	size_t i;

	assert(self);

	for (i=0; i<LEN(self->regs.r); i++) {
		if (i && !(i % 4)) {
			printf("\n");
		}
		printf("R%-2d = 0x%08"PRIx32" ", (int)i, self->regs.r[i]);
	}
	printf("\n");

	return 0;
}

static void*
thread_main(void *args)
{
	struct cpu *self = (struct cpu*)args;
	if (!args) return NULL;
	int ret;
	uint32_t pc;

	while (self->thread_running) {
		pc = self->regs.r[REG_PC];

		/* Run instruction */
		ret = cpu_step(self);
		if (ret) {
			log_warn("PC=%0"PRIx32" execution failed: %s", pc, strerror(ret));
		}

		/* TODO Check for breakpoints */
	}

	return NULL;
}
/* }}} */

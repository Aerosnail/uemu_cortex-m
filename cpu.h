#pragma once

#include "architecture.h"

struct cortex_m_specs {
	uint32_t cpuid;
};

int cpu_init(struct cpu **dst, MemBus *bus, const char *name, uint32_t cpuid);
int cpu_deinit(struct cpu *self);
int cpu_reset(struct cpu *cpu);
int cpu_step(struct cpu *cpu);

int cpu_exception_return(struct cpu *cpu);

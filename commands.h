#pragma once

#include "architecture.h"

int register_commands(const char *name, struct cpu *ctx);
int unregister_commands(const char *name);

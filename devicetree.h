#pragma once

#include <module.h>

#include "cpu.h"

int parse_dtb_prop(struct cortex_m_specs *dst, const struct dtb_property *prop,
                   find_phandle_t find_phandle, void *find_phandle_ctx);
